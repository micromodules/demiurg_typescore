cmake_minimum_required (VERSION 3.1)
set(CMAKE_CXX_STANDARD 14)

if(NOT DEFINED demiurg_typescore)
set(demiurg_typescore ON PARENT_SCOPE)

add_library(demiurg_typescore INTERFACE)
target_sources(demiurg_typescore INTERFACE
    "${CMAKE_CURRENT_SOURCE_DIR}/_Module.hpp"
	"${CMAKE_CURRENT_SOURCE_DIR}/IntegerTypes.hpp"		
	"${CMAKE_CURRENT_SOURCE_DIR}/PointerTypes.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/TypesPack.hpp"
	"${CMAKE_CURRENT_SOURCE_DIR}/Where.hpp"
	"${CMAKE_CURRENT_SOURCE_DIR}/TypeCategory.hpp"
	"${CMAKE_CURRENT_SOURCE_DIR}/InheritanceInfo.hpp"
	"${CMAKE_CURRENT_SOURCE_DIR}/ConstTemplateValues.hpp"
	"${CMAKE_CURRENT_SOURCE_DIR}/MemoryForValue.hpp")

endif()
