#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

#include <type_traits>

template<typename T_ValueType>
struct MemoryForValue
{
    T_ValueType& asValue() { return *reinterpret_cast<T_ValueType*>(&storage); }
    const T_ValueType& asValue() const { return *reinterpret_cast<const T_ValueType*>(&storage); }

    std::aligned_storage<sizeof(T_ValueType), alignof(T_ValueType)> storage;
};

template<typename T_ValueType>
T_ValueType& memoryAsValue(MemoryForValue<T_ValueType>& inMemoryForValue) {
    return *reinterpret_cast<T_ValueType*>(&inMemoryForValue);
}

#endif
