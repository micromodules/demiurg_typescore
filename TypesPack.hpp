#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

#include "ConstTemplateValues.hpp"

#include <utility>

template<typename ... T_Types>
struct TypesPack;

struct DummyType; //TODO: Move to separate file

// ----------------------------------------------------------------------------------------------------------------------------------------

template<typename ... T_Types>
struct ConcatTypes;

//Zero arguments
template<>
struct ConcatTypes<>
{
    using _ = TypesPack<>;
};

//One argument
template<typename ... T_PackTypes>
struct ConcatTypes<TypesPack<T_PackTypes ...>>
{
    using _ = TypesPack<T_PackTypes ...>;
};

template<typename T_Type>
struct ConcatTypes<T_Type>
{
    using _ = TypesPack<T_Type>;
};

//Two arguments
template<typename ... T_PackATypes, typename ... T_PackBTypes>
struct ConcatTypes<TypesPack<T_PackATypes ...>, TypesPack<T_PackBTypes ...>>
{
    using _ = TypesPack<T_PackATypes ..., T_PackBTypes ...>;
};

template<typename ... T_PackATypes, typename T_TypeB>
struct ConcatTypes<TypesPack<T_PackATypes ...>, T_TypeB>
{
    using _ = TypesPack<T_PackATypes ..., T_TypeB>;
};

template<typename T_TypeA, typename ... T_PackBTypes>
struct ConcatTypes<T_TypeA, TypesPack<T_PackBTypes ...>>
{
    using _ = TypesPack<T_TypeA, T_PackBTypes ...>;
};

template<typename T_TypeA, typename T_TypeB>
struct ConcatTypes<T_TypeA, T_TypeB>
{
    using _ = TypesPack<T_TypeA, T_TypeB>;
};

//More arguments
template<typename T_HeadType, typename ... T_TailTypes>
struct ConcatTypes<T_HeadType, T_TailTypes ...>
{
    using _ = typename ConcatTypes<typename ConcatTypes<T_HeadType>::_, typename ConcatTypes<T_TailTypes ...>::_>::_;
};

// ----------------------------------------------------------------------------------------------------------------------------------------

template<typename TNotTypesPack>
struct CheckIsTypesPack
{
    static_assert(getAlwaysFalse<TNotTypesPack>(), "Expected types pack");
};

template<typename ... TTypes>
struct CheckIsTypesPack<TypesPack<TTypes...>>
{
    using _ = TypesPack<TTypes...>;
};

#endif
