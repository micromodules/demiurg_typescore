#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

#include <cstddef>

template<typename T_Type> inline constexpr bool getAlwaysFalse() { return false; }
template<typename T_Type> inline constexpr bool getAlwaysTrue() { return true; }

template<typename T_ConstType, T_ConstType T_ConstValue>
struct Const {
    constexpr static T_ConstType _ = T_ConstValue;
};

template<bool T_ConstValue>
using BoolConst = Const<bool, T_ConstValue>;

#endif
