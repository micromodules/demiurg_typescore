#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

#include <cstdint>
#include <limits>

// ------------------------------------------------------ Types per size ------------------------------------------------------------------

template<uint8_t T_BytesNum, bool T_Signed> struct IntegerTypeImpl { };

template<> struct IntegerTypeImpl<1, false> { typedef uint8_t Type; };
template<> struct IntegerTypeImpl<1, true> { typedef int8_t Type; };

template<> struct IntegerTypeImpl<2, false> { typedef uint16_t Type; };
template<> struct IntegerTypeImpl<2, true> { typedef int16_t Type; };

template<> struct IntegerTypeImpl<3, false> { typedef uint32_t Type; };
template<> struct IntegerTypeImpl<3, true> { typedef int32_t Type; };
template<> struct IntegerTypeImpl<4, false> { typedef uint32_t Type; };
template<> struct IntegerTypeImpl<4, true> { typedef int32_t Type; };

template<> struct IntegerTypeImpl<5, false> { typedef uint64_t Type; };
template<> struct IntegerTypeImpl<5, true> { typedef int64_t Type; };
template<> struct IntegerTypeImpl<6, false> { typedef uint64_t Type; };
template<> struct IntegerTypeImpl<6, true> { typedef int64_t Type; };
template<> struct IntegerTypeImpl<7, false> { typedef uint64_t Type; };
template<> struct IntegerTypeImpl<7, true> { typedef int64_t Type; };
template<> struct IntegerTypeImpl<8, false> { typedef uint64_t Type; };
template<> struct IntegerTypeImpl<8, true> { typedef int64_t Type; };

template<uint8_t T_BytesNum, bool T_Signed>
using IntegerType = typename IntegerTypeImpl<T_BytesNum, T_Signed>::Type;

// ------------------------------------------------------ Biggest integer -----------------------------------------------------------------

template<bool T_Signed> struct BiggestIntegerTypeImpl { };

template<> struct BiggestIntegerTypeImpl<false> { typedef uint64_t Type; };
template<> struct BiggestIntegerTypeImpl<true> { typedef int64_t Type; };

template<bool T_Signed>
using BiggestIntegerType = typename BiggestIntegerTypeImpl<T_Signed>::Type;

typedef BiggestIntegerType<false> BiggestPossibleIntegerType;

// ---------------------------------------------------- Pointer sized integer -------------------------------------------------------------

using PointerIntegerType = IntegerType<sizeof(void*), false>;

#endif
