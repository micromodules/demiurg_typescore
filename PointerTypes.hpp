#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

#include "IntegerTypes.hpp"

typedef uint8_t DByteType;
typedef size_t DMemorySizeType;

using PointerIntegerType = IntegerType<sizeof(void*), false>;

template<typename T_TypeTo> struct Fit;

template<typename T_Type>
struct Fit<T_Type*>
{
    static T_Type* _(T_Type* inValue)                  { return inValue;   }
    static T_Type* _(const T_Type* inValue)            = delete;
    static T_Type* _(T_Type& inValue)                  { return &inValue;  }
    static T_Type* _(const T_Type& inValue)            = delete;
    static constexpr T_Type* _(std::nullptr_t)         { return nullptr; }
};

template<typename T_Type>
struct Fit<const T_Type*>
{
    static const T_Type* _(T_Type* inValue)            { return inValue;   }
    static const T_Type* _(const T_Type* inValue)      { return inValue;   }
    static const T_Type* _(T_Type& inValue)            { return &inValue;  }
    static const T_Type* _(const T_Type& inValue)      { return &inValue;  }
    static constexpr const T_Type* _(std::nullptr_t)   { return nullptr; }
};

template<typename T_Type>
struct Fit<T_Type&>
{
    static T_Type& _(T_Type* inValue)                  { return *inValue; }
    static T_Type& _(const T_Type* inValue)            = delete;
    static T_Type& _(T_Type& inValue)                  { return inValue;  }
    static T_Type& _(const T_Type& inValue)            = delete;
    static constexpr T_Type& _(std::nullptr_t)         { return *static_cast<T_Type*>(nullptr); }
};

template<typename T_Type>
struct Fit<const T_Type&>
{
    static const T_Type& _(T_Type* inValue)            { return *inValue; }
    static const T_Type& _(const T_Type* inValue)      { return *inValue; }
    static const T_Type& _(T_Type& inValue)            { return inValue;  }
    static const T_Type& _(const T_Type& inValue)      { return inValue;  }
    static constexpr const T_Type& _(std::nullptr_t)   { return *static_cast<const T_Type*>(nullptr); }
};

template<typename T_Type> static DByteType* asRaw(T_Type* inValue)                { return reinterpret_cast<DByteType*>(inValue);       }
template<typename T_Type> static const DByteType* asRaw(const T_Type* inValue)    { return reinterpret_cast<const DByteType*>(inValue); }
template<typename T_Type> static DByteType* asRaw(T_Type& inValue)                { return asRaw(&inValue);                             }
template<typename T_Type> static const DByteType* asRaw(const T_Type& inValue)    { return asRaw(&inValue);                             }

template<typename T_Type> static T_Type* rawAsPtr(DByteType* inValue)             { return reinterpret_cast<T_Type*>(inValue);          }
template<typename T_Type> static const T_Type* rawAsPtr(const DByteType* inValue) { return reinterpret_cast<const T_Type*>(inValue);    }
template<typename T_Type> static T_Type& rawAsRef(DByteType* inValue)             { return *rawAsPtr<T_Type>(inValue);                  }
template<typename T_Type> static const T_Type& rawAsRef(const DByteType* inValue) { return *rawAsPtr<T_Type>(inValue);                  }

#endif
