#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

enum class ETypeCategory : uint8_t {
    Simple,
    LReference,
    ConstLReference,
    RReference,
    Pointer,
    ConstPointer,
    Object
};

namespace GetTypeCategoryDetails
{
    template<typename T_Type> struct Impl
    {
    private:
        static constexpr bool kIsScalar{ std::is_scalar<T_Type>::value };
        static_assert(kIsScalar || std::is_object<T_Type>::value, "Incorrect usage case");
    public:
        static constexpr ETypeCategory _ = kIsScalar ? ETypeCategory::Simple : ETypeCategory::Object;
    };

    template<typename _T_Type> struct Impl<_T_Type&>{ static constexpr ETypeCategory _{ ETypeCategory::LReference }; };
    template<typename _T_Type> struct Impl<const _T_Type&>{ static constexpr ETypeCategory _{ ETypeCategory::ConstLReference }; };
    template<typename _T_Type> struct Impl<_T_Type&&>{ static constexpr ETypeCategory _{ ETypeCategory::RReference }; };
    template<typename _T_Type> struct Impl<_T_Type*>{ static constexpr ETypeCategory _{ ETypeCategory::Pointer }; };
    template<typename _T_Type> struct Impl<const _T_Type*>{ static constexpr ETypeCategory _{ ETypeCategory::ConstPointer }; };
};

template<typename T_Type> struct GetTypeCategory
{
    constexpr static ETypeCategory _{ GetTypeCategoryDetails::Impl<T_Type>::_ };
};

#endif
