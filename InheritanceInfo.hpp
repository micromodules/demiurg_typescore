#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

#include "Where.hpp"

#include <type_traits>
#include <cstdint>//for uint8_t

enum class ETypesRelation : uint8_t
{
    AIsNotRelatedWithB,
    AIsSameAsB,
    AIsBaseOfB,
    AIsChildOfB
};

template<typename T_Type>
constexpr bool isClass() {
    return std::is_class<T_Type>::value;
}

// ================================================ getTypesRelation =======================================================================

template<typename T_TypeA, typename T_TypeB, Where(!isClass<T_TypeA>() || !isClass<T_TypeB>())>
constexpr ETypesRelation getTypesRelation() = delete;

template<typename T_TypeA, typename T_TypeB, Where(isClass<T_TypeA>() && isClass<T_TypeB>())>
constexpr ETypesRelation getTypesRelation() {
    constexpr bool kIsSame = std::is_same<T_TypeA, T_TypeB>::value;
    return kIsSame ? ETypesRelation::AIsSameAsB :
            std::is_base_of<T_TypeA, T_TypeB>::value ? ETypesRelation::AIsBaseOfB :
            std::is_base_of<T_TypeB, T_TypeA>::value ? ETypesRelation::AIsChildOfB :
            ETypesRelation::AIsNotRelatedWithB;
}

template<typename T_TypeA, typename T_TypeB>
constexpr ETypesRelation isClassABaseOfClassB() { return (ETypesRelation::AIsBaseOfB == getTypesRelation<T_TypeA, T_TypeB>()); }
template<typename T_TypeA, typename T_TypeB>
constexpr ETypesRelation isClassAChildOfClassB() { return (ETypesRelation::AIsChildOfB == getTypesRelation<T_TypeA, T_TypeB>()); }

// ================================================ areRelatedClasses ======================================================================

template<typename T_TypeA, typename T_TypeB>
constexpr bool isClassARelatedWithClassB() {
    return (ETypesRelation::AIsNotRelatedWithB != getTypesRelation<T_TypeA, T_TypeB>());
}


//////////////TODO: Move this to separate file:

// ================================================ areRelatedClasses ======================================================================

template<typename T_TypeA, typename T_TypeB>
constexpr bool isAAssignableFromB() {
    return std::is_assignable<T_TypeA&, T_TypeB>::value;
}

#endif
