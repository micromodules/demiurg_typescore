#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_typescore__Version

#include <type_traits>

#define Where(...)\
    typename std::enable_if<__VA_ARGS__, int>::type = 0

#define WhereImpl(...)\
    typename std::enable_if<__VA_ARGS__, int>::type

//Workaround to fix GCC and Clang issue
// (you may see "https://stackoverflow.com/questions/13964447/why-compile-error-with-enable-if/26678178" for details)
template<typename TPseodependedType, bool T_ConstValue>
constexpr bool whereWorkaround() { return T_ConstValue; }

#define WhereWorkarounded(PseodependedType, ...)\
    Where((whereWorkaround<PseodependedType, __VA_ARGS__>()))

#define WhereWorkaroundedImpl(PseodependedType, ...)\
    WhereImpl((whereWorkaround<PseodependedType, __VA_ARGS__>()))

#endif
